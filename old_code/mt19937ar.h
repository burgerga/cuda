#ifndef MT19937AR_H
#define MT19937AR_H
#ifdef __cplusplus
extern "C" {
#endif

void init_genrand(unsigned long s);
void init_by_array(unsigned long init_key[], int key_length);
unsigned long genrand_int32(void);
long genrand_int31(void);
double genrand_real1(void);
double genrand_real2(void);
double genrand_real3(void);
double genfand_res53(void);
double genrand();
#ifdef __cplusplus
}
#endif
#endif
