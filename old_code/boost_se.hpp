#ifndef BOOST_SE_HPP
#define BOOST_SE_HPP
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include "boost_se.hpp"
#include "boost_po.hpp" //for definition of parameters
#include "md_cuda.cuh" //for definition of variables

namespace se = boost::serialization;
//adding serialization to the par and var classes
namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive & ar, Parameters & p, const unsigned int version);
	
template<class Archive>
void serialize(Archive & ar, Variables & v, const unsigned int version);

} // namespace serialization
} // namespace boost

class State {
   public:
	friend class se::access;
	Parameters *par;
	Variables *var;
	float **x;
	float **v;
	template<class Archive>
	void serialize(Archive & ar, const unsigned int version) ;
	State(Parameters *_par, Variables *_var, float **_x, float **_v) 
	   : par(_par), var(_var), x(_x), v(_v) {}
};

void save_state(State &st, const char * filename);
void restore_state(State &st, const char * filename);
#endif
