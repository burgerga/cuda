//C libraries
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <fstream>
#include <sys/time.h> 			//accurate timers

//boost libraries
#include <boost/asio/signal_set.hpp> 	//signal handling
#include <boost/filesystem.hpp> 	//for editing files and directories
#include <boost/format.hpp>		//format io

//thrust libraries (libraries like boost, but then for parallel)
#include <thrust/device_ptr.h> 
#include <thrust/inner_product.h> 
#include <thrust/for_each.h> 
#include <thrust/iterator/zip_iterator.h>
#include <thrust/iterator/constant_iterator.h>
#include <thrust/transform_reduce.h>

//local file from CUTIL
#include "../cuda_helper/cutil_math.h"

//custom files
#include "md_cuda.cuh"
#include "boost_po.hpp" //get configuration parameters
#include "boost_se.hpp" //resume (serialization) support
#include "mt19937ar.h" 	//random number generator
#include "util.hpp" //TODO some stuff
#include "cuda_util.cuh" //TODO includes cuda.h
#include "cpu_func.hpp" //TODO
#include "error_analysis.hpp" //code for computing standard error

using namespace std;

//alias
namespace bf = boost::filesystem;

//function prototypes
void initialize();
void d_integrate1( thrust::device_ptr<float4> d_x4, thrust::device_ptr<float4> d_v4, thrust::device_ptr<float4> d_a4);
void d_integrate2( thrust::device_ptr<float4> d_v4, thrust::device_ptr<float4> d_a4);
void d_calcAcceleration(thrust::device_ptr<float4> d_x4, thrust::device_ptr<float4> d_a4);
float d_calcKinEnergy(float *d_v);
float d_calcVirial(float *d_a);
void d_scaleMomentum( thrust::device_ptr<float4> d_v4);
void measure(ofstream & mf, ErrorAnalysis *pressure);
void handler( const boost::system::error_code& error, int signal_number);
void printForGl();

//TODO wrap function
__global__ void calculate_forces(float *d_x, float *d_a, float hl, int n);

inline double myrand();

/*
 * if b is a divisor of a returns a / b
 * else returns a / b rounded up
 */
uint iDivUp(uint a, uint b){
    return (a % b != 0) ? (a / b + 1) : (a / b);
}


//constructor for variables
Variables::Variables(){
	i = 0;
	cont = 1;
	avgpressure = 0;
	en = 0;
	virial = 0;
	etot = 0;
	nrfiles = 0;
} 

//global variables
Parameters par; //defined in boost_po.hpp
Variables var; //defined in md_cuda.cuh

//data on host
float *h_x, *h_v, *h_a;
//data on device (graphics card)
float *d_x, *d_v, *d_a;
//thrust device pointers (special pointers to data on device used in thrust code)
thrust::device_ptr<float4> d_x4, d_v4, d_a4;

int main(int argc, char **argv){
	
	//TODO set cpu or gpu execution
	bool gpu = true;

	//setup signal handling (in order to gracefully abort the program)
	boost::asio::io_service ios;
	boost::asio::signal_set signals(ios, SIGINT, SIGTERM);
	signals.async_wait(handler);
	
	//setup timer cpu functions
	struct timespec start, stop;
	float runtime;	

	//TODO setup random number generator
	unsigned long int seed = 1L;
#ifdef NDEBUG
	seed = time(NULL);
#endif
	cout << "seed = " << seed << "\n";
	init_genrand(seed);
	
	
	//get commandline parameters and execution mode
	int mode = get_opts(argc, argv, par);

	//setup cuda devices
	int num_devices;
	//myCudaSafeCall is defined in cuda_util.cuh, it checks for errors on the cuda device
	//get number of cuda devices available in the system
	myCudaSafeCall(cudaGetDeviceCount(&num_devices));
	//reserve space to store the properties of those devices
	cudaDeviceProp prop[num_devices];
	//loop over the devices
	for(int i = 0; i < num_devices; i++) {
		//get device properties and store them
		myCudaSafeCall(cudaGetDeviceProperties(&(prop[i]), i));
		//print names of devices
		cout << "device " << i << ": " << prop[i].name << endl;
	}
	//if more than one device choose the one specified in the configuration file
	//TODO choose device based on architecture and flops
	if(num_devices > 1) {
		cudaSetDevice(par.device);
		cout << "selecting device " << par.device << "\n" ;
	}

	//setup timer cuda functions (defined in cuda_util.cuh)
	//WARNING: CPU timing will not work since kernel launches are asynchronous
	EventTimer cet; 


	switch(mode){ //set execution mode
	   case 0: //normal: new simulation
		cout << "starting new simulation\n" ;
		//allocate host memory
		//float4 is used to allow for memory coalescence
		h_x = new float[4*par.n];
		h_v = new float[4*par.n];
		cout << "initializing... " ;
		clock_gettime(CLOCK_MONOTONIC, &start);
		//TODO setup initial conditions and velocity (show asynchronous execution)
		initialize();
		clock_gettime(CLOCK_MONOTONIC, &stop);
		cout << "done\n";
		runtime = 1e9*(stop.tv_sec - start.tv_sec ) + (stop.tv_nsec - start.tv_nsec);
		cout << "Run time CPU initialization:\t" << runtime*1e-6 << " ms." << endl;
		break;
	   case 1: //use configfile as starting configuration
		//WARNING not implemented yet
		cout << "starting new simulation using " << par.configfile << "\n";
		//TODO
		cerr << "error: function not implemented" << endl;
		return 1;
		//break;
	   case 2: //resuming simulation from resumefile
		//WARNING broken, not a priority right now
		cout << "resuming simulation from " << par.resumefile << "... ";
		State st(&par, &var, &h_x, &h_v);
		if( bf::exists( par.resumefile) )
			restore_state(st,par.resumefile.c_str());
		else {
			cerr << "error: could not open file " << par.resumefile << endl;
			exit(EXIT_FAILURE);
		}
		cout << "done\n" ;
		break;
	}


	//setup data structure to store pressure measurements (defined in error_analysis.hpp)
	ErrorAnalysis pressure(par.iter);
	
	//allocate host memory for acceleration
	h_a = new float[4*par.n];

	/* Prepare for output */
	//create output directory if it doesn't exist
	if(!bf::is_directory( par.outdir ))
		bf::create_directory( par.outdir );
	//determine file name for measurement output
	string ofname = str(boost::format("%s/measurements.dat") % par.outdir);
	//open output file for measurement, overwrite if exists
	ofstream mf;
	mf.open(ofname.c_str(),ios::trunc);
	if(!mf.is_open()){
		cerr << "error: could not open " << ofname << endl;
		exit(EXIT_FAILURE);
	}

	if(gpu){
		//setup device memory
		allocateDeviceArray((void**)&d_x, 4*par.n*sizeof(float));
		allocateDeviceArray((void**)&d_v, 4*par.n*sizeof(float));
		allocateDeviceArray((void**)&d_a, 4*par.n*sizeof(float));

		//setup device_ptrs to use previously allocated memory with thrust
		d_x4 = thrust::device_pointer_cast((float4 *)d_x);
		d_v4 = thrust::device_pointer_cast((float4 *)d_v);
		d_a4 = thrust::device_pointer_cast((float4 *)d_a);

		//copy arrays to device
		cout << "copying arrays from main memory to GPU memory... " ;
		clock_gettime(CLOCK_MONOTONIC, &start);
		copyArrayToDevice(d_x, h_x, 4*par.n*sizeof(float)); 
		copyArrayToDevice(d_v, h_v, 4*par.n*sizeof(float)); 
		clock_gettime(CLOCK_MONOTONIC, &stop);
		cout << "done\n";
		runtime = 1e9*(stop.tv_sec - start.tv_sec ) + (stop.tv_nsec - start.tv_nsec);
		cout << "Run time copying data to device:\t" << runtime*1e-6 << " ms." << endl;
	}

	/*TODO set printout mode and frequency in configuration file
	if(mode!=2){ //if simulation is not resumed print first configuration
		cout << "printing starting configuration... " ; 
		printForGl();
		cout << "done\n" ;
	}
	*/

	//setup timers for functions, set all to zero
	double time_int1, time_int2, time_acc, time_ke, time_vir, time_scaling;
	time_int1 = time_int2 = time_acc = time_ke = time_vir = time_scaling = 0;

	if(gpu){
		//TODO wrap and determine launch configuration, now done manually
		//see also occupancy calculator
		int tpb = 512; //threads per block
		//start timer
		cet.start();
		//calculate forces
		//d_calcAcceleration(d_x4, d_a4); //old function using thrust::for_each
		calculate_forces <<< iDivUp(par.n,tpb), tpb, tpb*sizeof(float4) >>>(d_x, d_a, (float) var.box[0]/2, par.n);
		//stop timer
		cet.stop();
		cout << "Run time GPU acceleration:\t " << cet.elapsed() << " ms." << endl;

		/* MAIN MD loop */
		cout << "starting main loop..." << endl ;
		//var.cont is set to false if interrupted by SIGTERM or SIGINT
		while(var.cont && var.i < par.nreq+par.iter){
			/* first velocity verlet integration step */
			cet.start();
			d_integrate1(d_x4, d_v4, d_a4);
			cet.stop();
			time_int1 += cet.elapsed();
			
			/* calculate acceleration */
			cet.start();
			//d_calcAcceleration(d_x4, d_a4);
			//TODO wrap
			calculate_forces <<<iDivUp(par.n,tpb), tpb, tpb*sizeof(float4) >>>(d_x, d_a, (float) var.box[0]/2, par.n);
			cet.stop();
			time_acc += cet.elapsed();

			/* second velocity verlet integration step */
			cet.start();
			d_integrate2( d_v4, d_a4);
			cet.stop();
			time_int2 += cet.elapsed();

			/* calculate virial */
			cet.start();
			var.virial = d_calcVirial(d_a);
			cet.stop();
			time_vir += cet.elapsed();

			/* calculate kinetic energy */
			cet.start();
			var.ke = d_calcKinEnergy(d_v);
			cet.stop();
			time_ke += cet.elapsed();
			
			/* scale velocities */
			cet.start();
			d_scaleMomentum(d_v4);
			cet.stop();
			time_scaling += cet.elapsed();
 			
			/* update time */
			var.t+=par.dt;

			/* do measurements */
			measure(mf, &pressure);

			/* write configuration to file */
			//TODO
			if(var.i % 100==0){
				copyArrayFromDevice(h_x, d_x, 4*par.n*sizeof(float)); 
				printForGl();
			}

			//increase iteration counter
			++var.i;

			//check for signals to terminate
			ios.poll();

		}//end while

	} else { //cpu MAIN

		/* calculate acceleration */
		clock_gettime(CLOCK_MONOTONIC, &start);
		calcAcceleration(h_x, h_a, par, &var);
		clock_gettime(CLOCK_MONOTONIC, &stop);
		runtime = 1e3*(stop.tv_sec - start.tv_sec ) + 1e-6*(stop.tv_nsec - start.tv_nsec);
		cout << "Run time CPU acceleration:\t" << runtime << " ms." << endl;

		//MD loop
		cout << "starting main loop..." << endl ;
		while(var.cont && var.i < par.nreq+par.iter){
			//if( var.i % 1000 == 0 ) cout << var.i << "\n";

			clock_gettime(CLOCK_MONOTONIC, &start);
			integrate1<float>(h_x, h_v, h_a, par, var);
			clock_gettime(CLOCK_MONOTONIC, &stop);
			time_int1 += 1e3*(stop.tv_sec - start.tv_sec ) + 1e-6*(stop.tv_nsec - start.tv_nsec);
			
			clock_gettime(CLOCK_MONOTONIC, &start);
			calcAcceleration(h_x, h_a, par, &var); //includes virial
			clock_gettime(CLOCK_MONOTONIC, &stop);
			time_acc += 1e3*(stop.tv_sec - start.tv_sec ) + 1e-6*(stop.tv_nsec - start.tv_nsec);

			clock_gettime(CLOCK_MONOTONIC, &start);
			integrate2(h_v, h_a, par, var);
			clock_gettime(CLOCK_MONOTONIC, &stop);
			time_int2 += 1e3*(stop.tv_sec - start.tv_sec ) + 1e-6*(stop.tv_nsec - start.tv_nsec);

			clock_gettime(CLOCK_MONOTONIC, &start);
			var.ke = calcKinEnergy(h_v, par);
			clock_gettime(CLOCK_MONOTONIC, &stop);
			time_ke += 1e3*(stop.tv_sec - start.tv_sec ) + 1e-6*(stop.tv_nsec - start.tv_nsec);

			clock_gettime(CLOCK_MONOTONIC, &start);
			scaleMomentum(h_v, var.ke, par);
			clock_gettime(CLOCK_MONOTONIC, &stop);
			time_scaling += 1e3*(stop.tv_sec - start.tv_sec ) + 1e-6*(stop.tv_nsec - start.tv_nsec);

			var.t+=par.dt; //update time

			measure(mf, &pressure);
			//TODO
			if(var.i % 100==0){
				printForGl();
			}

			++var.i;

			ios.poll();
		}//end while
	}

	

	//if interrupted save resume file
	//WARNING broken
	if(!var.cont) {
		string resumefile = par.outdir + "/resume.txt";
		cout << "saving resume state...";
		State st(&par, &var, &h_x, &h_v);
		save_state(st,resumefile.c_str());	
		cout << " done\n";
		cout << "creating symbolic link to resume file in current directory...";
		if(bf::exists( "./resume.txt" )) bf::remove( "./resume.txt" );
		bf::create_symlink( resumefile, "./resume.txt");
		cout << " done\n";
	//otherwise print timings
	} else {
		time_int1 /= var.i;
		time_int2 /= var.i;
		time_acc /= var.i;
		time_ke /= var.i;
		time_vir /= var.i;
		time_scaling /= var.i;
		
		cout << "Average time integrate1:\t " << time_int1 << " ms." << endl;
		cout << "Average time acceleration:\t " << time_acc << " ms." << endl;
		cout << "Average time integrate2:\t " << time_int2 << " ms." << endl;
		cout << "Average time kinetic energy:\t " << time_ke << " ms." << endl;
		cout << "Average time virial:\t\t " << time_vir << " ms." << endl;
		cout << "Average time momentum scaling:\t " << time_scaling << " ms." << endl;
	}

	//clearing host memory
	delete[] h_x;
	delete[] h_v;
	delete[] h_a;

	//closing output file
	mf.close();

	//clearing device memory
	freeDeviceArray((void*)d_x);
	freeDeviceArray((void*)d_v);
	freeDeviceArray((void*)d_a);

	//if terminated
	if(!var.cont){
		cout << "exiting now\n";
		return 1;
	}
	//if finished
	cout << "program completed successfully\n";
	//cout << boost::format("average pressure = %6f\n") % (var.avgpressure/par.iter);
	cout << boost::format("average pressure = %6f with error %6f \n") % (pressure.mean()) % (pressure.error()) ;
	return 0;
}

void initialize(){
	//sum of velocities in x y z direction
	double sumv[3] = {0.0, 0.0, 0.0};

	//set box size according to density
	var.box[0] = pow(par.n/par.density, 1.0/3.0);
	//assume cubic box
	var.box[2] = var.box[1] = var.box[0];
	//print box size
	cout << var.box[0] << endl;
	//box size implies cutoff so we need a correction factor
	//WARNING be sure to change this if you change the potential
	float rc = var.box[0]/2.0;
	//correction for LJ
	var.corr = 16.0/3.0 * M_PI *par.density*par.density * (2.0/3.0*pow(1/rc,9)-pow(1/rc,3));

	//find lowest perfect cube such that particles still fit
	int ndir = 2;
	while((ndir*ndir*ndir)<par.n) ++ndir;
	double dist = var.box[0]/ndir;
		
	int iix, iiy, iiz;
	iix = iiy = iiz = 0;
	for( int i = 0 ; i < par.n; ++i) {
		int ii = 4*i;
		h_x[ii] = (0.5+iix)*dist;
		h_x[ii+1] = (0.5+iiy)*dist;
		h_x[ii+2] = (0.5+iiz)*dist;
		h_x[ii+3] = 0;
		//draw random velocities from the gaussian
		h_v[ii]   = gauss(1.0); 
		h_v[ii+1] = gauss(1.0);
		h_v[ii+2] = gauss(1.0);
		//h_v[ii] = myrand()-0.5;
		//h_v[ii+1] = myrand()-0.5;
		//h_v[ii+2] = myrand()-0.5;
		h_v[ii+3] = 0;
		sumv[0]+=h_v[ii];
		sumv[1]+=h_v[ii+1];
		sumv[2]+=h_v[ii+2];
		++iix;
		if(iix==ndir){
			iix=0;
			++iiy;
			if(iiy==ndir){
				iiy=0;
				++iiz;
			}
		}
		
	}
	// calculate velocity center of mass
	sumv[0] /= par.n;
	sumv[1] /= par.n;
	sumv[2] /= par.n;
	//set the velocity center of mass to zero and compute the kinetic energy
	var.ke = 0;
	for(int i = 0; i < par.n; ++i){
		int ii = 4*i;
		h_v[ii] -= sumv[0];
		h_v[ii+1] -= sumv[1];
		h_v[ii+2] -= sumv[2];
		var.ke += h_v[ii]*h_v[ii]+h_v[ii+1]*h_v[ii+1]+h_v[ii+2]*h_v[ii+2];
	}
	var.ke /= 2;
	//compute instanteneous temperature
	float instatemp = var.ke/par.n*2./3.;
	//compute scalefactor needed to get the desired temperature
	float scalefactor = sqrtf(par.temp/instatemp);
	var.ke = 0.0;
	for(int i = 0; i < par.n; ++i){
		int ii = 4*i;
		h_v[ii] *= scalefactor;
		h_v[ii+1] *= scalefactor;
		h_v[ii+2] *= scalefactor;
		var.ke += h_v[ii]*h_v[ii]+h_v[ii+1]*h_v[ii+1]+h_v[ii+2]*h_v[ii+2];
	}
	var.ke /= 2;
}

/*
 * functor that does the first integration step
 * a functor is an object that can be used as function by overloading the operator()
 * the benefit of a functor is that you can initialize using a constructor 
 * and can be passed around so that you can use it in the algorithmic functions such as for_each
 */
struct integrate1_functor{
	float _dt;
	float3 _box;
	//constructor
	__host__ __device__ integrate1_functor(float dt, float3 box) : _dt(dt), _box(box) {}
	//the actual function
	//the function is templated to allow for any Tuple
	template <typename Tuple> __host__ __device__ void operator()(Tuple t){
		//extract tuple
		//get position from tuple
		volatile float4 x4 = thrust::get<0>(t);
		//get velocity from tuple
		volatile float4 v4 = thrust::get<1>(t);
		//get acceleration from tuple
		volatile float4 a4 = thrust::get<2>(t);
		//convert float4 to float3
		float3 x3 = make_float3(x4.x, x4.y, x4.z);
		float3 v3 = make_float3(v4.x, v4.y, v4.z);
		float3 a3 = make_float3(a4.x, a4.y, a4.z);
		//first velocity verlet integration step:  x = x + v*dt + 0.5*a*dt^2
		x3 += v3 * _dt + 0.5f * a3 * _dt * _dt;
		//apply periodic boundary conditions
		if(x3.x < 0.0f)		x3.x += _box.x;
		if(x3.x > _box.x)	x3.x -= _box.x;
		if(x3.y < 0.0f)		x3.y += _box.y;
		if(x3.y > _box.y)	x3.y -= _box.y;
		if(x3.z < 0.0f)		x3.z += _box.z;
		if(x3.z > _box.z)	x3.z -= _box.z;
		//update velocity:  v = v + 0.5*a*dt
		v3 += 0.5f * a3 * _dt;
		
		//store results back in tuple
		thrust::get<0>(t) = make_float4( x3, x4.w);	
		thrust::get<1>(t) = make_float4( v3, v4.w);	
	}
};
		
/*
 * the function that is called for the first integration step
 * first the integrate1 functor is constructed with the right time step and box size
 * then all particles are updated in parallel by using thrust::for_each,
 * which calls the functor for every particle
 */
void d_integrate1( thrust::device_ptr<float4> d_x4, thrust::device_ptr<float4> d_v4, thrust::device_ptr<float4> d_a4){
	//construct functor
	integrate1_functor integrate1_op(par.dt, make_float3(var.box[0],var.box[1],var.box[2]));
	//for each particle do
	thrust::for_each(
		/*combine velocity and acceleration together in a tuple
		and make them accesible with zip iterator */
		//define begin of the zip iterator
		thrust::make_zip_iterator(thrust::make_tuple(d_x4,d_v4,d_a4)),
		//define end of the zip iterator
		thrust::make_zip_iterator(thrust::make_tuple(d_x4+par.n,d_v4+par.n,d_a4+par.n)),
		//functor to be executed on the tuple
		integrate1_op
	);
}

/*
 * simple momentum scaling to get desired temperature
 */
void d_scaleMomentum( thrust::device_ptr<float4> d_v4){
	//calculate instanteneous temperature
	float instatemp = var.ke*2/(3*par.n);
	//compute scalefactor
	float scalefactor = sqrtf(par.temp/instatemp);
	//use thrust transform to multiply every particles velocity with the scalefactor in parallel
	thrust::transform(	
			//begin and end of the array to transform
			d_v4, d_v4+par.n,
			/*array to multiply with 
			(the use of constant_iterator avoids wasting space on a array filled with constant value) */
			thrust::make_constant_iterator(make_float4(scalefactor)),
			//the begin of the array in which to store the result (in this case overwrites the old array)
			d_v4,
			//the functor to call for the transformation, in this case we want to multiply two float4's
			thrust::multiplies<float4>() );
}

/*
 * functor for the second integration step
 * for explanation see the integrate1_functor
 */
struct integrate2_functor{
	float _dt;
	__host__ __device__ integrate2_functor(float dt) : _dt(dt) {}
	template <typename Tuple>
	__host__ __device__ void operator()(Tuple t){
		//extract tuple
		volatile float4 v4 = thrust::get<0>(t);
		volatile float4 a4 = thrust::get<1>(t);
		float3 v3 = make_float3(v4.x, v4.y, v4.z);
		float3 a3 = make_float3(a4.x, a4.y, a4.z);
		// v += 0.5*a*dt
		v3 += 0.5f * a3 * _dt;
		
		//store results
		thrust::get<0>(t) = make_float4( v3, dot(v3,v3));	
	}
};

/*
 * function to call for the second integration step
 * for explanation see d_integrate1
 */
void d_integrate2( thrust::device_ptr<float4> d_v4, thrust::device_ptr<float4> d_a4){
	integrate2_functor integrate2_op(par.dt);
	thrust::for_each(
		thrust::make_zip_iterator(thrust::make_tuple(d_v4,d_a4)),
		thrust::make_zip_iterator(thrust::make_tuple(d_v4+par.n,d_a4+par.n)),
		integrate2_op
	);
}

/*
 * WARNING please ignore, complex iterator stuff that is used to acces an array with strides in thrust
 */ 
struct transform_index_functor : public thrust::unary_function<int, int> {
	int _increment;
	int _start;
	__host__ __device__ transform_index_functor(int start, int increment) : _start(start), _increment(increment) {}
	__host__ __device__ int operator()(int index){
		return _start + _increment*index;
	}
};
	
	
/*
 * Calculate kinetic energy using awesome iterator shit
 * //TODO Will most likely change this because it is difficult to read 
 * and although elegant and memory efficient, the stride in memory access makes it slightly slower than storing an extra array
 */
float d_calcKinEnergy( float *d_v){
	thrust::device_ptr<float> d_tv(d_v);
	typedef thrust::counting_iterator<int> CountingIterator;
	typedef thrust::transform_iterator<transform_index_functor, CountingIterator> IndexIterator;
	typedef thrust::permutation_iterator<thrust::device_ptr<float>, IndexIterator> ValueIterator;
	CountingIterator index_first(0);
	CountingIterator index_last = index_first + par.n;
	transform_index_functor start3inc4(3,4);
	IndexIterator transformed_index_first(index_first, start3inc4);
	IndexIterator transformed_index_last(index_last, start3inc4);
	ValueIterator value_first(d_tv,transformed_index_first);
	ValueIterator value_last(d_tv,transformed_index_last);
	return 0.5f*thrust::reduce(value_first, value_last);
}

/*
 * same story as d_calcKinEnergy
 */
float d_calcVirial(float *d_a){
	thrust::device_ptr<float> d_ta(d_a);
	typedef thrust::counting_iterator<int> CountingIterator;
	typedef thrust::transform_iterator<transform_index_functor, CountingIterator> IndexIterator;
	typedef thrust::permutation_iterator<thrust::device_ptr<float>, IndexIterator> ValueIterator;
	CountingIterator index_first(0);
	CountingIterator index_last = index_first + par.n;
	transform_index_functor start3inc4(3,4);
	IndexIterator transformed_index_first(index_first, start3inc4);
	IndexIterator transformed_index_last(index_last, start3inc4);
	ValueIterator value_first(d_ta,transformed_index_first);
	ValueIterator value_last(d_ta,transformed_index_last);
	return 0.5f*thrust::reduce(value_first, value_last);
}
	

/*
 * WARNING not used in the program
 * row wise parallelized acceleration functor
 * similar to the integrate functors
 */
struct calcAccelerationPart : public thrust::unary_function<thrust::tuple<int,float4>, float4>{
	float _hl;
	float4 _x4i;
	float3 _x3i;
	int _pi;
	__host__ __device__ calcAccelerationPart(float hl, int i, float4 x4i) : _hl(hl), _pi(i), _x4i(x4i) {
		_x3i= make_float3(_x4i.x, _x4i.y, _x4i.z);
	}
	__host__ __device__ float4 operator()(thrust::tuple<int,float4> t){
		int pj = thrust::get<0>(t);
		if(_pi == pj) return make_float4(0.0f);
		volatile float4 x4j = thrust::get<1>(t);
		float3 x3j = make_float3(x4j.x, x4j.y, x4j.z);
		float3 xr3 = _x3i - x3j;
		//nearest image convention
		if(xr3.x < - _hl) xr3.x += 2*_hl;
		if(xr3.x >   _hl) xr3.x -= 2*_hl;
		if(xr3.y < - _hl) xr3.y += 2*_hl;
		if(xr3.y >   _hl) xr3.y -= 2*_hl;
		if(xr3.z < - _hl) xr3.z += 2*_hl;
		if(xr3.z >   _hl) xr3.z -= 2*_hl;
		float r2 = dot(xr3,xr3);
		float r6i = 1/(r2*r2*r2);
		float ff = 48*r6i*(r6i-0.5f);
		return make_float4(ff/r2*xr3,0.0f);
	}
};

/*
 * WARNING not used
 * calls the above functor on every element to compute acceleration
 */
void d_calcAcceleration(thrust::device_ptr<float4> d_x4, thrust::device_ptr<float4> d_a4){
	thrust::counting_iterator<int> first(0);
	thrust::counting_iterator<int> last = first + par.n;
	for( int i = 0; i < par.n; ++i){
		calcAccelerationPart calcAccPart_op((float)(var.box[0]/2), i,* (d_x4+i));
		*(d_a4+i) = thrust::transform_reduce(
				thrust::make_zip_iterator(thrust::make_tuple(first,d_x4)),
				thrust::make_zip_iterator(thrust::make_tuple(last,d_x4+par.n)),
				calcAccPart_op,
				make_float4(0.0f),
				thrust::plus<float4>());
	}
}
	
/*
 * function that is used for measuring
 * writes some output to files and the screen
 */
void measure(ofstream & mf, ErrorAnalysis *pressuredata){ 
	//compute pressure using the virial
	double pressure = par.density*var.ke*2/(3*par.n) + var.virial/(3*var.box[0]*var.box[1]*var.box[2]);
	//correct for implied cutoff by box size
	pressure += var.corr;
	//output to file
	mf   << boost::format("%d %.3f %f %f\n") % (var.i+1) % var.t % var.ke % pressure ;
	if((var.i+1) % 100 == 0){
		cout << boost::format("%d %.3f %f %f\n") % (var.i+1) % var.t % var.ke % pressure << flush;
	
	}
	if(var.i >= par.nreq)
		pressuredata->push(pressure);
}

/*
 * inline function to determine which random number generator to use
 * //TODO change to mersenne twister from boost
 * if changed make sure to also change seed setting method appropiately
 */
inline double myrand(){
	return genrand();
}

/*
 * signal handler
 */
void handler( const boost::system::error_code& error, int signal_number){
	if (!error) {
		//print signal number received
		cout << "\nsignal " << signal_number << " received\n" << flush;
		cout << "trying to exit gracefully\n" << flush;
		//set to false so that current iteration finishes and then write resume files
		var.cont = false;
	}
}

/* 
 * print for opengl
 * //TODO set print output mode in boost program options
 */
void printForGl(){
	string ofname;
	ofstream os;
	//franks opengl
	if(par.output_frank == true){
		//set output name
		ofname = str(boost::format("%s/config_frank.sph") % par.outdir);
		//if first output overwrite any existant file
		if(var.nrfiles == 0) os.open(ofname.c_str(),ios::trunc);
		//else append
		else os.open(ofname.c_str(),ios::app);
	//joost opengl
	} else {
		ofname = str(boost::format("%s/config_%04d") % par.outdir % var.nrfiles);
		os.open(ofname.c_str(),ios::trunc);
	}
	if(os.is_open()){
		if(par.output_frank == true){
			os << "&" << par.n << "\n";
			os << var.box[0] << " " << var.box[1] << " "  << var.box[2] << "\n";
		}
		else {
			os << "# " << var.box[0] << " " << 0.0 << " " << 0.0 << "\n";
			os << "# " << 0.0 << " " << var.box[1] << " " << 0.0 << "\n";
			os << "# " << 0.0 << " " << 0.0 << " " << var.box[2] << "\n";
			//os << "# " << 0.0 << " " << 0.0 << " " << 0.1 << " ";
		}
		for( int i = 0; i < par.n; ++i){
			int ii = 4*i;
			if(par.output_frank == true)
				os << "a" << " " ;
			else
				os << 1.0 << " " ; //type of particle
			os << h_x[ii] << " " << h_x[ii+1] << " " << h_x[ii+2] << " " ; //cartesion coordinates
			//os << h_x[ii] << " " << h_x[ii+1] << " " << 0.0 << " " ; //cartesion coordinates
			if(par.output_frank == true)
				os << 0.5 << "\n" ;
			else {
				os << 1.0 << " " << 0.0 << " " << 0.0 << " " << 0.0 << " " ; //rotation quaternion
				os << 1.0 << " " << 1.0 << "\n"; //circ and insc sphere radius
			}
		}
		os.close();
	} else
		cerr << "error: could not open file " << ofname << endl ;
	++var.nrfiles;
}
 
/* 
 * WARNING not used
 * anderson thermostat cpu code
 */
void thermostat(int offset = 4){
	static float sigma;
	static bool set = false;
	if(!set){
		sigma = sqrtf(par.temp);
		set = true;
	}

	//thermostat
	var.ke = 0;
	//TODO for( int i =0; i < par.n; ++i ){
	for( int i =0; i < 0; ++i ){
		int ii= offset*i;
		if( myrand() < par.nu*par.dt ){
			h_v[ii]  =gauss(sigma);	
			h_v[ii+1]=gauss(sigma);
			h_v[ii+2]=gauss(sigma);
		}
		var.ke += h_v[ii]*h_v[ii]+h_v[ii+1]*h_v[ii+1]+h_v[ii+2]*h_v[ii+2];
	}	
	var.ke /= 2;
	var.etot = (var.en+var.ke)/par.n;
}

/****************8
 * HERE start the cuda kernels for computing the acceleration
 * they are  borrowed from ...
 * and some adjustments are made to make them suitable for my program
 */

/*
 * returns particle-particle interaction
 */
__device__ float4 ppInteraction(float4 x4_pi, float4 x4_pj, float4 a4_pi, float hl ){
	float3 xr3;
	xr3.x = x4_pi.x - x4_pj.x;
	xr3.y = x4_pi.y - x4_pj.y;
	xr3.z = x4_pi.z - x4_pj.z;
	if(xr3.x < - hl) xr3.x += 2*hl;
	if(xr3.x >   hl) xr3.x -= 2*hl;
	if(xr3.y < - hl) xr3.y += 2*hl;
	if(xr3.y >   hl) xr3.y -= 2*hl;
	if(xr3.z < - hl) xr3.z += 2*hl;
	if(xr3.z >   hl) xr3.z -= 2*hl;
	float r2 = dot(xr3,xr3);
	//checks if the particles are the same (distance == 0) or if one particle is dummy (distance == inf)
	if(r2==0.0f || isinf(r2)) return a4_pi; 
	float r6i = 1/(r2*r2*r2);
	float ff = 48*r6i*(r6i-0.5f);
	a4_pi += make_float4(ff/r2*xr3,ff);
	return a4_pi;
}

/*
 * calculate ppInteraction for a tile in the n x n acceleration matrix
 */
__device__ float4 tile_calculation(float4 myPos, float4 accel, float hl){
	extern __shared__ float4 shPos[];
	for( int i = 0; i < blockDim.x; ++i){
		//shPos[i] is accesed by all threads at the same time, but because it is in shared memory, broadcast is used
		accel = ppInteraction(myPos, shPos[i], accel, hl);
	}
	return accel;
}

/*
 * kernel that calculates acceleration
 * //TODO still needs to be wrapped with launch configuration to clean up code
 * this kernel is executed in parallel so many threads are running this at the same time
 * the thread id that is calculated determines which particle is updated
 */
__global__ void calculate_forces(float *d_x, float *d_a, float hl, int n){
	//compute global thread id	
	int gtid = blockIdx.x * blockDim.x + threadIdx.x; 
	//reserve some space in shared memory to store information that must be accessed by all threads at the same time
	extern __shared__ float4 shPos[];
	//acces float array by using a float4 pointer (for memory coalescence)
	float4 *d_x4 = (float4 *) d_x;
	float4 *d_a4 = (float4 *) d_a;
	//reserve space to store own position
	float4 myPos;
	//set acceleration to zero
	float4 acc = make_float4(0.0f);
	//if the thread is responsible for a real (not dummy) particle load my position
	if(gtid < n) myPos = d_x4[gtid];
	//do calculation on a tile (the size of which is determined by the launch configuration)
	for( int tile = 0; tile < gridDim.x ; ++tile){
		//see which particle I am responsible for in this tile
		int idx = tile * blockDim.x + threadIdx.x;
		//if it is a real particle store it's position in shared memory
		//TODO use ()?:
		if(idx < n)	shPos[threadIdx.x] = d_x4[idx];
		//if it is a dummy set the particles position in shared memory to inf
		else shPos[threadIdx.x] = make_float4(logf(0));
		//synchronize to make sure that we're al at the same point of execution, needed for memory broadcast
		__syncthreads();
		//if responsible for a real particle calculate acceleration due to the particles in the tile
		if(gtid < n) acc = tile_calculation(myPos, acc, hl);
		//sync again
		__syncthreads();
	}
	//if real particle store acceleration
	if(gtid < n) d_a4[gtid] = acc;
}
	
	
