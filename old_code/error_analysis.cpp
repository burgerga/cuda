#include "error_analysis.hpp"

#include <boost/bind.hpp>
#include <boost/ref.hpp>
#include <algorithm> //for_each
#include <cmath>
#include <iostream>

using namespace boost::accumulators;

ErrorAnalysis::ErrorAnalysis(int _nrSamples, int _nrBins) : nrSamples(_nrSamples) ,nrBins(_nrBins) {
	if(nrBins < 10) 
		std::cerr << "warning: number of bins might be too low for error estimate" << std::endl;
	if(nrSamples % nrBins != 0) {
		std::cerr << "error: nrSamples not a multiple of number of bins" << std::endl;
		exit(EXIT_FAILURE);
	}
	bins.resize(nrBins);
	samplesPerBin = nrSamples / nrBins;
	done = false;
	count = 0;
}
void ErrorAnalysis::process() {
	if(count < nrSamples) {
		std::cerr << "not all samples added" << std::endl;
		exit(EXIT_FAILURE);
	}
	std::for_each( bins.begin(), bins.end(), 
		boost::bind<void>( boost::ref(acc), boost::bind<double>(std::divides<double>(), _1, samplesPerBin)));
	done = true;
	return;
}
void ErrorAnalysis::push(double value) {
	bins[count / samplesPerBin] += value;
	count++;
}
double ErrorAnalysis::mean() {
	if(done == false) process();
	return boost::accumulators::mean(acc);
}
double ErrorAnalysis::variance() {
	if(done == false) process();
	//second sample central moment / variance: Sum[(data-Mean[data])^2]/N
	//return boost::accumulators::variance(acc);
	//unbiased estimator of the variance: Sum[(data-Mean[data])^2]/(N-1)
	return boost::accumulators::variance(acc)*nrBins/(nrBins-1);
}
double ErrorAnalysis::error() {
	if(done == false) process();
	//return standard error of the mean: Sqrt[variance/nrsamples]
	return sqrt(variance()/10);
}

