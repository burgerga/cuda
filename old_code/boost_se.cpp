#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <iostream>
#include <fstream>

#include <cstdio>

#include "boost_po.hpp" //for definition of parameters
#include "md_cuda.cuh" //for definition of variables
#include "boost_se.hpp"

using namespace std;

namespace boost {
namespace serialization {

template<class Archive>
void serialize(Archive & ar, Parameters & p, const unsigned int version){
	ar & p.outdir;
	ar & p.n;
	//ar & p.delta;
	//ar & p.deltaV;
	//ar & p.deltaR;
	//ar & p.deltaPe;
	ar & p.density;
	//ar & p.pressure;
	ar & p.temp;
	//ar & p.melt_start_p;
	//ar & p.melt_stop_p;
	//ar & p.melt_dp;
	ar & p.iter;
	ar & p.nreq;
	ar & p.dt;
	ar & p.output_frank;
}
	
template<class Archive>
void serialize(Archive & ar, Variables & v, const unsigned int version){
	ar & v.i;
	ar & v.t;
	ar & v.avgpressure;
	ar & v.box;
	ar & v.en;
	ar & v.ke;
	ar & v.box;
	ar & v.nrfiles;
}

} // serialization
} //boost

template<class Archive>
void State::serialize(Archive & ar, const unsigned int version) {
	ar & *par;
	ar & *var;
	if(Archive::is_loading::value){
		*x = new float[4*par->n];
		*v = new float[4*par->n];
	}
	float *tx = *x;
	float *tv = *v;
	for(int i = 0; i < 4*par->n; ++i){
		ar & tx[i] & tv[i];
	}
}

void save_state(State &st, const char * filename) {
	//open archive
	ofstream ofs(filename);
	boost::archive::text_oarchive oa(ofs);
	oa << st;
}

void restore_state(State &st, const char * filename) {
	ifstream ifs(filename);
	boost::archive::text_iarchive ia(ifs);
	ia >> st;
}
