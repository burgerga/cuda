#ifndef CUDA_UTIL_CUH
#define CUDA_UTIL_CUH

//#include <cutil_inline.h> //replaced by own error functions
#include <cstdlib> //for EXIT_FAILURE

// Define this to turn on cuda timing
//#define CUDA_TIME
// Define this to turn on error checking
//#define CUDA_ERROR_CHECK

#define myCudaSafeCall( err ) __myCudaSafeCall( err, __FILE__, __LINE__ )
#define myCudaCheckError()    __myCudaCheckError( __FILE__, __LINE__ )

inline void __myCudaSafeCall( cudaError err, const char *file, const int line ) {
	#ifdef CUDA_ERROR_CHECK
	if ( cudaSuccess != err ) {
		fprintf( stderr, "cudaSafeCall() failed at %s:%i : %s\n", file, line, cudaGetErrorString( err ) );
		exit(EXIT_FAILURE);
	}
	#endif
	return;
}

inline void __myCudaCheckError( const char *file, const int line ) {
	#ifdef CUDA_ERROR_CHECK
	cudaError err = cudaGetLastError();
	if ( cudaSuccess != err ) {
		fprintf( stderr, "cudaCheckError() failed at %s:%i : %s\n",
		file, line, cudaGetErrorString( err ) );
		exit(EXIT_FAILURE);
	}

	// More careful checking. However, this will affect performance.
	// Comment away if needed.
	err = cudaDeviceSynchronize();
	if( cudaSuccess != err ) {
		fprintf( stderr, "cudaCheckError() with sync failed at %s:%i : %s\n", file, line, cudaGetErrorString( err ) );
		exit(EXIT_FAILURE);
	}
	#endif
	return;
}

//introduce cuda timer
class EventTimer {
public:
	EventTimer() : mStopped(true) {
		cudaEventCreate(&mStart);
		cudaEventCreate(&mStop);
	}
	~EventTimer() {
		cudaEventDestroy(mStart);
		cudaEventDestroy(mStop);
	}
	void start(cudaStream_t s = 0) {
		#ifdef CUDA_TIME
		if(!mStopped) {
			cerr << "error: EventTimer already running" ;
			exit(EXIT_FAILURE);
		}
		myCudaSafeCall(cudaEventRecord(mStart, s)); mStopped = false; 
		#endif
		return;
	}
	void stop(cudaStream_t s = 0)  {
		#ifdef CUDA_TIME
		if(mStopped) {
			cerr << "error: EventTimer already stopped" ;
			exit(EXIT_FAILURE);
		}
		myCudaSafeCall(cudaEventRecord(mStop, s)); mStopped = true;
		#endif
		return;
	}
	float elapsed() {
		#ifdef CUDA_TIME
		if (!mStopped) {
			cerr << "error: EventTimer still running" ;
			exit(EXIT_FAILURE);
		}
		myCudaSafeCall(cudaEventSynchronize(mStop));
		float elapsed = 0.0f;
		myCudaSafeCall(cudaEventElapsedTime(&elapsed, mStart, mStop));
		return elapsed;
		#else
		return 0.0f;
		#endif
	}
private:
	bool mStopped;
	cudaEvent_t mStart, mStop;
};

/*
 * SOME USEFUL FUNCTIONS
 */

void allocateDeviceArray(void **devPtr, size_t size)
{
	//cutilSafeCall(cudaMalloc(devPtr, size));
	myCudaSafeCall(cudaMalloc(devPtr, size));
}

void freeDeviceArray(void *devPtr)
{
	//cutilSafeCall(cudaFree(devPtr));
	myCudaSafeCall(cudaFree(devPtr));
}

void deviceSync()
{
	//cutilSafeCall(cudaDeviceSynchronize());
	myCudaSafeCall(cudaDeviceSynchronize());
}

void copyArrayToDevice(void* device, const void* host, int size)
{
	//cutilSafeCall(cudaMemcpy(device , host, size, cudaMemcpyHostToDevice));
	myCudaSafeCall(cudaMemcpy(device , host, size, cudaMemcpyHostToDevice));
}

void copyArrayFromDevice(void* host, const void* device, int size)
{ 
	//cutilSafeCall(cudaMemcpy(host, device, size, cudaMemcpyDeviceToHost));
	myCudaSafeCall(cudaMemcpy(host, device, size, cudaMemcpyDeviceToHost));
}
#endif
