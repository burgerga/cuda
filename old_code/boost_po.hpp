#ifndef BOOST_PO_HPP
#define BOOST_PO_HPP

#include <string>

using namespace std;

//DONT FORGET TO UPDATE SERIALIZATION IF CHANGED
class Parameters {
   public:
	string outdir; 		//parameter file
	int n;	 		//total number of co
	int device;
	//double delta; 		//maximum distance the particles can move
	//double deltaV; 		//maximum difference in volume
	//double deltaR; 	//maximum rotation angle
	//double deltaPe; 	//maximum rotation angle
	double density; 	//desired initial density
	//double pressure; 	//desired initial pressure in beta*P*sigma^3
	double temp;		//desired initial temperature
	double nu;		//collision rate
	//double melt_start_p;	//starting pressure for melting 
	//double melt_stop_p; 	//stopping pressure for melting
	//double melt_dp;		//pressurestep for melting
	int iter; 		//number of measuring iterations
	int nreq; 		//number of equilabration iterations
	double dt;		//timestep
	bool output_frank; 
	string configfile;
	string resumefile;
	Parameters(){};
}; 

int get_opts(int argc, char **argv, Parameters & par);

#endif
