// multiply.cu : Simple scalar-vector product (similar to the MPI example)
// Computes only one element in a single thread and uses N threads
// Kees Lemmens, Mar 2009
//

#include <stdio.h>
#include <sys/time.h>
#include <cuda.h>

// This runs on the CPU :
float multiplyOnHost(float *a, long N)
{
   long i;
   struct timeval ti1,ti2;
   float runtime;
   
   fprintf(stderr,"Now computing on CPU : \n");
   gettimeofday(&ti1,NULL);        // read starttime into t1
   
   for(i=0;i<N;i++)
     a[i] *= 3;
   
   gettimeofday(&ti2,NULL);        // read endtime into t2
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);
   fprintf(stderr,"Run time CPU : %f microsecs.\n",runtime * 1e6);
   
   return runtime;
}

// This is the kernel that runs on the GPU :
__global__ void multiplyKernel(float *b)
{
   int i = blockIdx.x * blockDim.x + threadIdx.x;
   b[i] *= 3;
}

// This part also runs on the CPU (and not on the GPU !) :
float multiplyOnDevice(float *b_h, long N)
{
   float *a_d;   // Pointer to device array
   struct timeval ti1,ti2,ti3;
   int nBlocks;
   float runtime;
   
   fprintf(stderr,"Now computing on GPU : \n");
   
   gettimeofday(&ti1,NULL);        // read starttime into t1
   
   cudaMalloc((void **) &a_d, N * sizeof(float));   // Allocate array on device
   cudaMemcpy(a_d, b_h, N * sizeof(float), cudaMemcpyHostToDevice);
   
   // Do the actual calculation on N threads inside the GPU device:
   nBlocks = N;
   if(nBlocks >= 65535)
   {
      fprintf(stderr,"Maximum number of blocks exceeded : %d >= 65535 !\n", nBlocks);
      exit(1);
   }
   gettimeofday(&ti2,NULL);        // read starttime into t2
   multiplyKernel <<< nBlocks, 1 >>> (a_d);
   cudaThreadSynchronize(); /* Make sure all threads finished before stopping clock */
   
   gettimeofday(&ti3,NULL);        // read endtime into t3
   
   runtime = (ti3.tv_sec - ti2.tv_sec ) + 1e-6*(ti3.tv_usec - ti2.tv_usec);
   fprintf(stderr,"Run time GPU (using %d blocks and 1 threads/block ) : %f microsecs.\n",
	   nBlocks, runtime * 1e6);
   
   // Retrieve result from device and store it in host array
   cudaMemcpy(b_h, a_d, sizeof(float)*N, cudaMemcpyDeviceToHost);
   
   cudaFree(a_d);

   gettimeofday(&ti3,NULL);        // read endtime into t3
   runtime = (ti3.tv_sec - ti1.tv_sec ) + 1e-6*(ti3.tv_usec - ti1.tv_usec);
   fprintf(stderr,"Run time GPU including memory copy : %f microsecs.\n",
	   runtime * 1e6);
   
   return runtime;
}

// main routine that executes on the host
int main(int argc, char **argv)
{
   float *a_h,*b_h;      // Pointers to host arrays
   long N = 4096L;       // Number of elements in arrays (problemsize)
   long i;
   float ts,tp;
   
   if(argc >=2 )
     sscanf(argv[1],"%ld",&N);

   fprintf(stderr,"Starting with N=%d and 1 thread/block ...\n",N);
   
   // Allocate arrays on host
   a_h = (float *)malloc(N * sizeof(float));
   b_h = (float *)malloc(N * sizeof(float));

   // Initialize host arrays 
   for (i=0; i<N; i++)
     b_h[i] = a_h[i] = (float)i;
   
   // Run sequential algorithm on CPU
   ts = multiplyOnHost(a_h, N);
   
   // Run parallel algorithm on GPU
   tp = multiplyOnDevice(b_h, N);
   
   // check results
#if (DEBUG > 0)
   for (i=0; i<N; i++)
   {
      float f = fabs(a_h[i] - b_h[i]);
      printf("CPU: %f , GPU: %f , diff: %f\n",a_h[i],b_h[i],f);
   }
#endif
   
   printf("Ratio execution time GPU/CPU: %f\n",tp/ts);

   free(a_h);
   exit(0);
}

