multiply2 :

N=10^3 time GPU/CPU : 86000
N=10^5 time GPU/CPU : 1200
N=10^7 time GPU/CPU : 7

multiply3 :

N=10^3 time GPU/CPU : 1300
N=10^5 time GPU/CPU : 5
N=10^7 time GPU/CPU : 0.02   (so GPU is here (much) faster than CPU)

KL, Oct 2010
