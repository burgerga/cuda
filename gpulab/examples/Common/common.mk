# Hacked together from the Nvidia makefiles and common.mk
# Stripped a lot of "rubbish" and force source, object and target to
# be simply in the current dir.
#
# Kees Lemmens, Oct 2008, Feb 2010, Sept 2010 (cuda 3.1)

################################################################################
# Rules and targets
################################################################################

.SUFFIXES : .cu .cu_dbg_o .c_dbg_o .cu_rel_o .c_rel_o

# detect if 32 bit or 64 bit system
HP_64 = $(shell uname -m | grep 64)

CUDAHOME := /usr/local/cuda

SDK_INSTALL_PATH := /home/gerhard/NVIDIA_GPU_Computing_SDK
CUDA_INSTALL_PATH := /usr/local/cuda

ifdef cuda-install
	CUDA_INSTALL_PATH := $(cuda-install)
endif

# Basic directory for SDK: change to location where you installed the SDK
LIBDIR     := $(SDK_INSTALL_PATH)/C/lib
COMMONDIR  := $(SDK_INSTALL_PATH)/C/common/linux

# Compilers
NVCC       := nvcc 
CC         := gcc
CXX        := g++
LINK       := g++ -fPIC

# Includes
INCLUDES  += -I. -I$(CUDA_INSTALL_PATH)/include -I$(COMMONDIR)/inc

# Warning flags
CXXWARN_FLAGS := \
	-Wall \
	-Wimplicit \
	-Wswitch \
	-Wformat \
	-Wchar-subscripts \
	-Wparentheses \
	-Wmultichar \
	-Wtrigraphs \
	-Wpointer-arith \
	-Wcast-align \
	-Wreturn-type \
	-Wno-unused-function

CWARN_FLAGS := $(CXXWARN_FLAGS) \
	-Wstrict-prototypes \
	-Wmissing-prototypes \
	-Wmissing-declarations \
	-Wnested-externs \
	-Wmain

# Compiler-specific flags
NVCCFLAGS := 
CFLAGS    := $(CWARN_FLAGS)
CXXFLAGS  := $(CXXWARN_FLAGS)

# Common flags
COMMONFLAGS += $(INCLUDES) -DUNIX -D_GLIBCXX_GCC_GTHR_POSIX_H # MOD KL, Feb 2009

# Debug/release configuration
ifeq ($(DEBUG),1)
	COMMONFLAGS += -g
	NVCCFLAGS   += -D_DEBUG -G
else 
	COMMONFLAGS += -O3
	# MOD KL, Feb 2010 to fix a nvcc cuda bug in combination with gcc 4.4.3 :
	NVCCFLAGS   += --compiler-options -fno-strict-aliasing --compiler-options -fno-inline
	CFLAGS      += -fno-strict-aliasing
endif

# OpenGL is used or not (if it is used, then it is necessary to include GLEW)
OPENGLLIB := -lGL -lGLU -lglut
ifeq ($(USEGLLIB),1)
	ifeq "$(strip $(HP_64))" ""
		OPENGLLIB += -lGLEW
	else
		OPENGLLIB += -lGLEW_x86_64
	endif
endif

ifeq ($(USECUDPP), 1)
	ifeq "$(strip $(HP_64))" ""
		CUDPPLIB := -lcudpp
	else
		CUDPPLIB := -lcudpp64
	endif

	CUDPPLIB := $(CUDPPLIB)$(LIBSUFFIX)
endif

# Libs
LIB := -L$(CUDA_INSTALL_PATH)/lib64 -L$(CUDA_INSTALL_PATH)/lib -L$(LIBDIR) -L$(COMMONDIR)/lib

ifeq ($(USEDRVAPI),1)
   LIB += -lcuda -lcudart ${OPENGLLIB} $(CUDPPLIB)
else
   LIB += -lcudart ${OPENGLLIB} $(CUDPPLIB)
endif

ifeq ($(USECUFFT),1)
   LIB += -lcufft
endif

ifeq ($(USECUBLAS),1)
   LIB += -lcublas
endif

ifeq ($(USECUSPARSE),1)
   LIB += -lcusparse
endif

# Lib/exe configuration
ifneq ($(STATIC_LIB),)
	LINKLINE  = ar qv $(TARGET) $(OBJS) 
else
#	LIB += -lcutil$(LIBSUFFIX)   # Non-existent in cuda 3.1
	LINKLINE  = $(LINK) -o $(TARGET) $(OBJS) $(LIB)
endif

################################################################################
# Check for input flags and set compiler flags appropriately
################################################################################

# Add cudacc flags
NVCCFLAGS += $(CUDACCFLAGS)

# Add common flags
NVCCFLAGS += $(COMMONFLAGS)
CFLAGS    += $(COMMONFLAGS)
CXXFLAGS  += $(COMMONFLAGS)

################################################################################
# Set up object files
################################################################################

OBJS +=  $(patsubst %.c,%.c_o,$(notdir $(CFILES)))
OBJS +=  $(patsubst %.cpp,%.cpp_o,$(notdir $(CCFILES)))
OBJS +=  $(patsubst %.cu,%.cu_o,$(notdir $(CUFILES)))

################################################################################
# Rules
################################################################################
%.c_o : %.c
	$(VERBOSE)$(CC) $(CFLAGS) -DDEBUG=$(DEBUG) -o $@ -c $<

%.cpp_o : %.cpp
	$(VERBOSE)$(CXX) $(CXXFLAGS) -DDEBUG=$(DEBUG) -o $@ -c $<

%.cu_o : %.cu
	$(VERBOSE)$(NVCC) -DDEBUG=$(DEBUG) -o $@ -c $< $(NVCCFLAGS)

tidy :
	$(VERBOSE)rm -f *# *~ core

