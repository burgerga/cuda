/*
 Simple test to see how the FFT routines perform by running many of them
 after each other on the same CUDA card.

 Doesn't use any input or output, only internally generated random data.
  
 Only argument is the size of the random vector to be created.
 Kees Lemmens, Nov 2008
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <sys/time.h>

/* Includes, cuda */
#include "cuda.h"
#include "cufft.h"

/* Matrix size */
//#define N 1024
#define BATCH 1
//#define DEBUG 1

//#define N 2048
//#define DEBUG 1

// extern void ldebug(int, char*, ...);  /* for C++ code */
extern "C" void ldebug(int, char*, ...); /* for standard C code */

int readdim(int argc, char *argv[])
{
   ldebug(1,"readdim entered ...\n");
   
   if(argc <= 1)
   {
      printf("Please supply a dimension.\n");
      exit(1);
   }
   
   return atoi(argv[1]);
}

int fillCVector(cufftComplex *a,int n)
{
   int i;

   ldebug(1,"fillCVector entered ...\n");
   
   for(i=0;i<n;i++)
   {
      a[i].x = rand()/RAND_MAX; // real part
      a[i].y = 0.0;             // imaginary part
   }
   return 0;
}

cufftComplex *initCVector(int n)
{
   cufftComplex *ptr;
   
   ldebug(1,"initCVector entered ...\n");
   
   ptr = (cufftComplex *) calloc(n, sizeof(cufftComplex));
   
   if(ptr == NULL)
   {
      fprintf(stderr,"Malloc for complex vector on host failed !\n");
      exit(1);
   }
   return ptr;
}

void checkCudaError(cudaError_t status, char *error)
{
   if (status != cudaSuccess)
   {
      fprintf (stderr, "Cuda : %s\n",error);
      exit(EXIT_FAILURE);
   }
}

void checkCufftResult(cufftResult_t status, char *error)
{
   if (status != CUFFT_SUCCESS) {
      fprintf (stderr, "Cuda CUFFT : %s\n",error);
      exit(EXIT_FAILURE);
   }
}

cufftComplex *initCufftCVector(int n)
{
   cufftComplex *ptr;
   cudaError_t status;

   ldebug(1,"initCufftCVector entered ...\n");

   status = cudaMalloc( (void**)&ptr, n * sizeof(cufftComplex));
   checkCudaError(status,"Malloc for complex vector on device failed !");
   
   return ptr;
}

void copytoCufftCVector(cufftComplex *d_a, cufftComplex *h_a, int n)
{
   cudaError_t status;
   
   ldebug(1,"copytoCufftCVector entered ...\n");

   status = cudaMemcpy(d_a, h_a, n *sizeof(cufftComplex), cudaMemcpyHostToDevice);
   checkCudaError(status," Copy complex data to device failed !");
}

void copyfromCufftCVector(cufftComplex *h_a, cufftComplex *d_a, int n)
{
   cudaError_t status;
   
   ldebug(1,"copyfromCufftCVector entered ...\n");

   status = cudaMemcpy(h_a, d_a, n *sizeof(cufftComplex), cudaMemcpyDeviceToHost);
   checkCudaError(status," Copy complex data from device failed !");
}

void freeCuda(cufftComplex *a)
{
   cudaError_t status;
   
   status = cudaFree(a);
   checkCudaError(status,"Memory free error !");
}

/* Main */
int main(int argc, char *argv[])
{    
   struct timeval ti1,ti2;
   double runtime;
   int dim;
   int x;
   int nrruns=50;
   cufftHandle plan;
   cufftResult status;
   
   cufftComplex *h_At;
   cufftComplex *h_Af;
   cufftComplex *d_At = 0;
   cufftComplex *d_Af = 0;
   
   dim = readdim(argc--,argv++);
   
   fprintf(stderr,"\n");
   
   /* Allocate host memory for the vectors */
   h_At = initCVector(dim);
   h_Af = initCVector(dim);

   /* Allocate device memory for the vectors */
   d_At = initCufftCVector(dim*BATCH);
   d_Af = initCufftCVector(dim*BATCH);

   status = cufftPlan1d(&plan, dim, CUFFT_C2C, BATCH);
   checkCufftResult(status,"Plan init has failed !");
   
   fillCVector(h_At,dim);
   
   gettimeofday(&ti1,NULL);        /* read starttime in t1 */
   
   /* Initialize the device vector with the host vector */
   copytoCufftCVector(d_At,h_At,dim);
   
   /* Do the actual Fourier transform */
   status = cufftExecC2C(plan,d_At,d_Af,CUFFT_FORWARD);
   checkCufftResult(status,"Kernel execution error !");

   /* Read the result back */
   copyfromCufftCVector(h_Af,d_Af,dim);

   gettimeofday(&ti2,NULL);        /* read endtime in t2 */
   
   runtime = (ti2.tv_sec - ti1.tv_sec) + 1e-6*(ti2.tv_usec-ti1.tv_usec);

   fflush(stderr);
   fprintf(stderr,"\nCufft : run time for 1 run = %f secs (including memory copy from/to device).\n",runtime);

   gettimeofday(&ti1,NULL);        /* read starttime in t1 */
   
   /* Performs multiple operations using cufft */
   for(x=0;x<nrruns;x++)
   {
      status = cufftExecC2C(plan,d_At,d_Af,CUFFT_FORWARD);
      checkCufftResult(status,"Kernel execution error !");
   }
   
   gettimeofday(&ti2,NULL);        /* read endtime in t2 */
   
   runtime = (ti2.tv_sec - ti1.tv_sec) + 1e-6*(ti2.tv_usec-ti1.tv_usec);

   fflush(stderr);
   fprintf(stderr,"\nCufft : run time for %d runs = %f secs (%f secs per run).\n",
	   nrruns,runtime,runtime/nrruns);

   ldebug(1,"Cleaning up host memory ...\n");
   free(h_At);
   free(h_Af);
   
   ldebug(1,"Cleaning up device memory ...\n");
   freeCuda(d_At);
   freeCuda(d_Af);
   
   ldebug(1,"Destroying plan ...\n");
   status = cufftDestroy(plan);
   checkCufftResult(status,"Plan destroy has failed !");
   
   return EXIT_SUCCESS;
}
