/* Matrix Multiplication example on CuDa using block partioning and subMatrices in shared memory
 * 
 * Copies data from global memory to shared memory to achieve a significant
 * performance boast.
 * 
 * Performs the operation : C = A * B
 * 
 * - 'A' and 'B' are two square matrices with size dim x dim;
 * - 'C' is the resulting matrix;
 * 
 * Kees Lemmens, Nov 2008, Sept 2010
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "cuda.h"

#define MAXSHAREDMEM 16000

// Nr of threads per threadblock or blocksize (MAX = 16x16 for NVS290)
#define MAXBLOCKSIZE 16

/* These are simple routines stored in a separate source file as they are not really
 * important for understanding this example. */ 

extern void fillMatrix(float *a, int n, int m, int offset);
extern void showMatrix(char *name, float *a, int n, int m);
extern void checkCudaError(cudaError_t status, char *error);
extern float *initHostMatrix(int n, int m);
extern float *initCudaMatrix(int n, int m);
extern void copytoCudaMatrix(float *d_a, float *h_a, int n, int m);
extern void copyfromCudaMatrix(float *h_a, float *d_a, int n, int m);
extern void freeCudaHost(float *a);
extern void freeCudaDevice(float *a);

__global__ void d_matrixProdCuda(float *d_A, float *d_B, float *d_C, int dim)
{
   float Cs = 0;
   int bofx, bofy; // Block offset
   long x, y, z;
   int i;
   
   // Each  threadblock computes one submatrix of size blocksize x blocksize, and each
   // thread in a threadblock computes the product for only one element.
   // After each submatrix the threads advance to the next submatrix, until all submatrices
   // are processed and the running total for each element/thread is the final result.

   __shared__ char array[MAXSHAREDMEM];
   // reserve space so we can use "dynamic" shared memory
   // maximum allowed is 16384 - 0x28 internal stuff on a FX1700
   // 
   // And now allocate subMatrices in shared memory (which is much faster than global) :
   // Note that thios trick doesn't allow for multidimensional arrays as in that case we dont
   // know the row offset !

   float *As = (float *)array;
   float *Bs = (float *)&As[blockDim.x * blockDim.y];
   // TODO : check for buffer overflow !!!

   bofx = blockDim.x * blockIdx.x;
   bofy = blockDim.y * blockIdx.y;
   
   for(i = 0; i < dim/blockDim.x; i++)
   {
      // Now let each thread copy their own element from A and B to shared memory As and Bs :
      As[(threadIdx.y * blockDim.y) + threadIdx.x] = d_A[ i * blockDim.y + (bofy + threadIdx.y) * dim + threadIdx.x];
      Bs[(threadIdx.y * blockDim.y) + threadIdx.x] = d_B[(i * blockDim.x + threadIdx.y) * dim + bofx + threadIdx.x];
      
      // Wait until they are all finished copying :
      __syncthreads();
      
      // Each thread computes the product for a single element of the (sub)matrix C inside the block :
      for(z=0; z < blockDim.x; z++)
	Cs += As[(threadIdx.y * blockDim.y) + z] * Bs[(z * blockDim.x) + threadIdx.x];
      
      __syncthreads();
   }
   
   // Compute block and thread offset and then store result in global memory :
   x = bofx + threadIdx.x; // row index for this thread
   y = bofy + threadIdx.y; // col index for this thread
   d_C[y * dim + x] = Cs;
}

void matrixProdCuda(float *d_A, float *d_B, float *d_C, int dim, int threadsPerBlock)
{
   cudaError_t status;
   
   // Use "gridSize" blocks and "blockSize" threads per block :
   dim3 gridSize(dim/threadsPerBlock, dim/threadsPerBlock);
   dim3 blockSize(threadsPerBlock, threadsPerBlock);

   d_matrixProdCuda <<<gridSize, blockSize>>> (d_A, d_B, d_C, dim);
   
   status = cudaGetLastError();
   checkCudaError(status,"Kernel execution error !");
}

int main(int argc, char** argv)
{
   struct timeval ti1,ti2;
   double runtime;
   int dim = 4;
   float *h_A,     *h_B,     *h_C;
   float *d_A = 0, *d_B = 0, *d_C = 0;
   int threadsPerBlock;
   
   if(argc >=2 )
     sscanf(argv[1],"%ld",&dim);
   
   threadsPerBlock = (dim > MAXBLOCKSIZE ? MAXBLOCKSIZE : (dim/4)*4);
   fprintf(stderr,"Matrix product with dim = %d, blocksize = %d*%d\n",dim,threadsPerBlock,threadsPerBlock);
   
   gettimeofday(&ti1,NULL);/* read starttime in t1 */
   
   /* Allocate host memory for the matrices */
   h_A = initHostMatrix(dim,dim);
   h_B = initHostMatrix(dim,dim);
   h_C = initHostMatrix(dim,dim);
   
   fillMatrix(h_A,dim,dim, 0);
   fillMatrix(h_B,dim,dim, 10);
   
   /* Allocate device memory for the matrices */
   d_A = initCudaMatrix(dim,dim);
   d_B = initCudaMatrix(dim,dim);
   d_C = initCudaMatrix(dim,dim);
   
   /* Initialize the device matrices with the host matrices */
   copytoCudaMatrix(d_A,h_A,dim,dim);
   copytoCudaMatrix(d_B,h_B,dim,dim);
   
   /* Clear last error */
   cudaGetLastError();
   
   /* Performs operation using Cuda kernel above */
   matrixProdCuda(d_A, d_B, d_C, dim, threadsPerBlock);
   
   /* Read the result back */
   copyfromCudaMatrix(h_C,d_C,dim,dim);
   
   gettimeofday(&ti2,NULL); /* read endtime in t2 */
   runtime = (ti2.tv_sec - ti1.tv_sec) + 1e-6*(ti2.tv_usec - ti1.tv_usec);
   
   showMatrix("C",h_C,dim,dim);
   
   /* Memory clean up */
   freeCudaDevice(d_A);
   freeCudaDevice(d_B);
   freeCudaDevice(d_C);
   
   freeCudaHost(h_A);
   freeCudaHost(h_B);
   freeCudaHost(h_C);
   
   fflush(stderr);
   fprintf(stderr,"\nCuda : run time = %f secs.\n",runtime);
   
   return EXIT_SUCCESS;
}
