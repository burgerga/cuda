/* Simple matrix Multiplication example using CuDa and 1D matrices on the host
 * 
 * Performs the operation : C = A * B
 * 
 * - 'A' and 'B' are two square matrices with size dimxdim;
 * - 'C' is the resulting matrix;
 * 
 * Kees Lemmens, Nov 2008
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "cuda.h"

#define MAXBLOCKSIZE 16

/* These are simple routines stored in a separate source file as they are not really
 * important for understanding this example. */ 

extern void fillMatrix(float *a, int n, int m, int offset);
extern void showMatrix(char *name, float *a, int n, int m);
extern void checkCudaError(cudaError_t status, char *error);
extern float *initHostMatrix(int n, int m);
extern float *initCudaMatrix(int n, int m);
extern void copytoCudaMatrix(float *d_a, float *h_a, int n, int m);
extern void copyfromCudaMatrix(float *h_a, float *d_a, int n, int m);
extern void freeCudaHost(float *a);
extern void freeCudaDevice(float *a);

// Just an example of a GPU device function called from the kernel :
__device__ float mult(float a, float b)
{
   return a * b;
}

__global__ void d_oddloop(float *d_A, float *d_B, float *d_C,
			int dim, int blockSize)
{
   float Cs;
   long x,y,z;
   
   // Each block computes one horizontal strip of height blockSize, and each
   // thread in a block computes the product for only one single row.

#if (DEBUG >= 2)
   fprintf(stderr,"thread Index %d, block Index %d\n",blockIdx.x,threadIdx.x);
#endif
   
   y = (blockIdx.x * blockSize) + threadIdx.x; // column index

   for(x=1; x < dim; x+=2) // walk over a complete row
   {
      Cs = 0; // running total
      for(z=0; z < dim; z++) // walk over a complete column
      {
	 Cs += mult(d_A[y * dim + z], d_B[z * dim + x]);
      }
      
      // Compute block and thread offset and then store element in global memory :
      d_C[y * dim + x] = Cs;
   }
}

__global__ void d_evenloop(float *d_A, float *d_B, float *d_C,
			 int dim, int blockSize)
{
   float Cs;
   long x,y,z;

   // Each block computes one horizontal strip of height blockSize, and each
   // thread in a block computes the product for only one single row.

#if (DEBUG >= 2)
   fprintf(stderr,"thread Index %d, block Index %d\n",blockIdx.x,threadIdx.x);
#endif
   
   y = (blockIdx.x * blockSize) + threadIdx.x; // column index

   for(x=0; x < dim; x+=2) // walk over a complete row
   {
      Cs = 0; // running total
      for(z=0; z < dim; z++) // walk over a complete column
      {
	 Cs += mult(d_A[y * dim + z], d_B[z * dim + x]);
      }
      
      // Compute block and thread offset and then store element in global memory :
      d_C[y * dim + x] = Cs;
   }
}

void matrixProdCuda(float *d_A, float *d_B, float *d_C, int dim, int blockSize)
{
   cudaError_t status;
   int nBlocks;
   
   nBlocks = dim/blockSize; // use strips of dim * dim/blockSize for each thread
   
   d_evenloop <<<nBlocks, blockSize>>> (d_A, d_B, d_C, dim, blockSize);
   d_oddloop  <<<nBlocks, blockSize>>> (d_A, d_B, d_C, dim, blockSize);
   
   status = cudaGetLastError();
   checkCudaError(status,"Kernel execution error !");
}

int main(int argc, char** argv)
{    
   struct timeval ti1,ti2;
   double runtime;
   int dim = 4;
   float *h_A,     *h_B,     *h_C;
   float *d_A = 0, *d_B = 0, *d_C = 0;
   int blockSize;

   if(argc >=2 )
     sscanf(argv[1],"%ld",&dim);

   blockSize = (dim > MAXBLOCKSIZE ? MAXBLOCKSIZE : (dim/4)*4);
   fprintf(stderr,"Matrix product with dim = %d, blocksize = %d\n",dim,blockSize);
      
   gettimeofday(&ti1,NULL);/* read starttime in t1 */

   /* Allocate host memory for the matrices */
   h_A = initHostMatrix(dim,dim);
   h_B = initHostMatrix(dim,dim);
   h_C = initHostMatrix(dim,dim);
   
   fillMatrix(h_A,dim,dim, 0);
   fillMatrix(h_B,dim,dim, 10);
   
   /* Allocate device memory for the matrices */
   d_A = initCudaMatrix(dim,dim);
   d_B = initCudaMatrix(dim,dim);
   d_C = initCudaMatrix(dim,dim);
   
   /* Initialize the device matrices with the host matrices */
   copytoCudaMatrix(d_A,h_A,dim,dim);
   copytoCudaMatrix(d_B,h_B,dim,dim);
   
   /* Clear last error */
   cudaGetLastError();
   
   /* Performs operation using Cuda kernel above */
   matrixProdCuda(d_A, d_B, d_C, dim, blockSize);
   
   /* Read the result back */
   copyfromCudaMatrix(h_C,d_C,dim,dim);
   
   gettimeofday(&ti2,NULL); /* read endtime in t2 */
   
   // showMatrix("A",h_A,dim,dim);
   // showMatrix("B",h_B,dim,dim);

   showMatrix("C",h_C,dim,dim);
   
   /* Memory clean up */
   freeCudaHost(h_A);
   freeCudaHost(h_B);
   freeCudaHost(h_C);
   
   freeCudaDevice(d_A);
   freeCudaDevice(d_B);
   freeCudaDevice(d_C);
   
   runtime = (ti2.tv_sec - ti1.tv_sec ) + 1e-6*(ti2.tv_usec - ti1.tv_usec);

   fflush(stderr);
   fprintf(stderr,"\nCuda : run time = %f secs.\n",runtime);
   
   return EXIT_SUCCESS;
}
