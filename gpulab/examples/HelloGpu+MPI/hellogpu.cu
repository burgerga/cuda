// Simple example of how to mix MPI code and Cuda code using both C++ and C :
// Kees Lemmens, March 2011

#include <stdio.h>
#include <stdarg.h>
#include <cuda.h>

#define TEST "THIS IS ME, THE GPU !"
#define NRTPBK  22 // size of string TEST+1 / NRBLKS
#define NRBLKS   1 // Note : NRTPBK * NRBLKS = strlen(TEST)

int checkCudaError(cudaError_t status, const char *format, ...)
{
   if (status != cudaSuccess)
   {
      va_list p;
      
      va_start(p,format);
      vprintf(format, p);
      va_end(p);
      return(EXIT_FAILURE);
   }
   return 0;
}

__global__ void routineOnGPU(char *string_d)
{
   char string_k[] = TEST;
   int myid = blockIdx.x * blockDim.x + threadIdx.x;
   
   // Each thread copies it's own char from local to global memory :
   string_d[myid] = string_k[myid];
}

int hellogpu(int procid,int vmsize)
{
   cudaError_t status; // for errors on the GPU
   char *string_h;     // pointer to host   memory
   char *string_d;     // pointer to device memory
   int exit;
   int size = strlen(TEST) + 1;
   
   // allocate arrays on host and on device
   string_h = (char *)calloc(size,1); // empty string
   status = cudaMalloc((void **) &string_d, size);
   exit = checkCudaError(status,"Malloc on GPU device failed for slave %d !\n", procid);
   if(exit > 0) return exit;
   
   // clear string_d on GPU device by copying empty string_h over it
   status = cudaMemcpy(string_d, string_h, size, cudaMemcpyHostToDevice);
   exit = checkCudaError(status,"Sending to GPU device failed for slave %d !\n", procid);
   if(exit > 0) return exit;
   
   // Start a simple GPU kernel on NRTPBK threads and NRBLKS blocks
   routineOnGPU <<< NRBLKS, NRTPBK >>> (string_d); 
   
   // retrieve data from GPU device: string_d to string_h
   cudaMemcpy(string_h, string_d, size, cudaMemcpyDeviceToHost);
   exit = checkCudaError(status,"Receiving from GPU device failed for slave %d !\n", procid);
   if(exit > 0) return exit;
   
   printf("Hello World on slave process id %d : %s\n",procid, string_h);
   
   // cleanup memory
   cudaFree(string_d);
   free(string_h);
   
   return 0;
}
