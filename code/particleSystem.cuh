/**
	@file particleSystem.cuh
	@brief Extern declaration of function in particleSystem_cuda.cu so that they can be called from the ParticleSystem class.
	@see particleSystem_cuda.cu
*/

extern "C"
{
    void cudaInit(int cmdArgDevice);
    void checkDeviceCapabilities(int major, int minor);

    void allocateDeviceArray(void **devPtr, int size);
    void freeDeviceArray(void *devPtr);

    void copyArrayFromDevice(void *host, const void *device, int size);
    void copyArrayToDevice(void *device, const void *host, int offset, int size);
    void backupArrayDevice(void *dest, const void *source, int size);


    void setParameters(SimParams *hostParams);

    void integrateSystem1(float *pos,
                         float *vel,
                         float *acc,
                         float deltaTime,
			float3 boxSize,
                         uint numParticles);

	void accelerateNBody(float *pos, float *acc, float *virial, uint numParticles);
	void accelerateCelllists(float *newAcc, float *virial, float *sortedPos, uint *gridParticleIndex, uint *cellStart, uint *cellEnd, uint numParticles);
	void potentialCelllists(float *potentialEnergy, float *sortedPos, uint *gridParticleIndex, uint *cellStart, uint *cellEnd, uint numParticles);
	

    void integrateSystem2(float *vel,
                         float *acc,
                         float *kin,
                         float deltaTime,
                         uint numParticles);

    void calcHash(uint  *gridParticleHash,
                  uint  *gridParticleIndex,
                  float *pos,
                  int    numParticles);

    void reorderDataAndFindCellStart(uint  *cellStart,
                                     uint  *cellEnd,
                                     float *sortedPos,
                                     uint  *gridParticleHash,
                                     uint  *gridParticleIndex,
                                     float *oldPos,
                                     uint   numParticles,
                                     uint   numCells);

    void sortParticles(uint *dGridParticleHash, uint *dGridParticleIndex, uint numParticles);

	float thrustReduce(float *array, uint numParticles);

	void scaleArray(float *array, float scalefactor, uint length);

}
