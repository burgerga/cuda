/**
 *	@file particleSystem.h
 *	@brief Interface for the ParticleSystem class.
 *	@see ParticleSystem
 *	@see particleSystem.cpp
 */
#pragma once
#ifndef __PARTICLESYSTEM_H__
#define __PARTICLESYSTEM_H__

#include <vector_functions.h>
#include <string>

#include "particles_kernel.cuh"
#include "boost_rng.hpp"

/**
 *	@brief Class that contains the particle system and methods to update it.
 *	Stores the current state of the particle system and defines the function that can update it
 */
class ParticleSystem {
public:
	/// Constructor
	ParticleSystem(uint numParticles, uint3 gridSize, float3 boxSize, float temperature, std::string outdir, float cutoff);
	/// Destructor
	~ParticleSystem();

	/// enum for the initial particle placement
	enum ParticleConfig {
		CONFIG_RANDOM, ///< particles are placed in the box randomly
		CONFIG_GRID, ///< particles are placed on a grid
	};

	/// enum to select the right array
	enum ParticleArray {
		POSITION, ///< select position array
		VELOCITY, ///< select velocity array
		ACCELERATION, ///< select acceleration array
	};

	/// enum to set the simulation mode
	enum SimulationMode {
		NBODY, ///< use nbody approach
		CELLLIST, ///< use celllist approach
	};

	void startMDSimulation(uint equilibration, uint iterations, SimulationMode mode, float deltaTime);
	void startMCSimulationNVT(uint equilibration, uint iterations, SimulationMode mode, float deltaTime);
	void startMCSimulationNPT(uint equilibration, uint equilibration2, uint iterations, SimulationMode mode, float deltaTime, float pressure, float dlnV);
	void MDstep(SimulationMode mode, float deltaTime);
	
	void resetPos(ParticleConfig config);
	void resetVel();
	
	float *getArray(ParticleArray array);
	void setArray(ParticleArray array, const float *data, int start, int count);
	
	/// returns the number of particles
	int getNumParticles() const {
		return m_numParticles;
	}
	
	void dumpGrid();
	void dumpParticles(uint start, uint count);
	void writeForGL();
	
	/// returns the grid size
	uint3 getGridSize() {
		return m_params.gridSize;
	}
	
	/// returns the origin of the simulation box
	float3 getWorldOrigin() {
		return m_params.worldOrigin;
	}
	
	/// returns the cell size
	float3 getCellSize() {
		return m_params.cellSize;
	}
	
protected: //methods
	
	void _initialize(uint numParticles);
	void _finalize();

	void initGrid(uint *size, float spacing);

protected: //data
	
	bool m_bInitialized; ///< bool to see if system is initialized
	bool m_velscaling; ///< bool to switch velocity scaling in MD NVT
	uint m_numParticles; ///< number of particles

	/**
	 *	@name CPU data for particles
	 */
	//@{
	float *m_hPos; ///< position
	float *m_hVel; ///< velocity
	float *m_hAcc; ///< acceleration
	//@}
	
	/**
	 *	@name GPU data for particles
	 */
	//@{
	float *m_dPos; ///<  position
	float *m_dSortedPos; ///< sorted positions
	float *m_dVel; ///<  velocity
	float *m_dAcc; ///<  acceleration
	float *m_dKineticEnergy; ///< kinetic energy contributions
	float *m_dPotentialEnergy; ///< potential energy contributions
	float *m_dVirial; ///< virial contributions
	//@}

	/**
	 *	@name GPU grid Data (for sorting)
	 */
	//@{
	uint *m_dGridParticleHash; 	///< grid hashes
	uint *m_dGridParticleIndex; 	///< grid indeces
	uint *m_dCellStart; 		///< starting particles for cells
	uint *m_dCellEnd; 		///< ending particles for cells
	//@}

	/**
	 *	@name CPU grid Data for dumping (error checking)
	 */
	//@{
	uint *m_hParticleHash; 	///< grid hashes 
	uint *m_hCellStart;	///< starting particles for cells
	uint *m_hCellEnd;  	///< ending particles for cells


	/**
	 *	@name Parameters
	 */
	//@{
	SimParams m_params; ///< cpu instance of ::SimParams, to be put in constant memory
	uint3 m_gridSize; ///< size of grid
	float3 m_boxSize; ///< size of simulation box
	uint m_numGridCells; ///< number of cells in the grid
	float m_temperature; ///< temperature of simulation
	std::string m_outdir; ///< directory to write output to
	float m_cutoff; ///< cutoff radius
	float m_pressureCorrection; ///< correction for the pressure when using cutoff
	float m_potentialEnergyCorrection; ///< correction (takes care of the shifted part in 'truncated and shifted)
	float m_density; ///< number density of the system
	//@}

	/**
	 *	@name Variables
	 */
	//@{
	float m_instantTemperature; ///< instantenuous temperature
	float m_virial; ///< virial of the system
	float m_potentialEnergy; ///< potential energy of the system
	float m_kineticEnergy; ///< kinetic energy of the system
	float m_pressure; ///< temperature of the system
	//@}

	BoostRNG m_rng; ///< boost random number generator

};

#endif // __PARTICLESYSTEM_H__
