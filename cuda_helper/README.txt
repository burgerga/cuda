README for cuda helper

several files:

cuda_error_check.cuh:	provides a wrapper around cuda functions that checks if a cudacommand was executed succesfully
cuda_timer.cuh: 	provides an easy to use timer for cuda functions, 
			do not use cpu timers for cuda functions since kernel launches are asynchronous!
			(meaning control is immediately passed back to the host, resulting in weird timings)
cutil_math.h: 		stolen from NVIDIA, provides some useful math code for the custom defined types such as float4
select_device.h:	stolen from NVIDIA, provides functions to select the fastest device or to select a device of choice
