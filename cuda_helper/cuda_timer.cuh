/** 	
	@file cuda_timer.cuh
	@brief implements the EventTimer class.
	@see EventTimer
*/
#pragma once
#ifndef CUDA_TIMER_CUH
#define CUDA_TIMER_CUH

#include "cuda_errorcheck.cuh"
#include <iostream>

/**
	@class EventTimer	
	@brief A timer class for CUDA. 
    	Allows the user to simply create, start and stop a timer,
	without having to worry about managing the cuda events
*/
class EventTimer {
public:
	/** Constructor. Initializes EventTimer::mStopped to true and creates the EventTimer::mStart and EventTimer::mStop cudaEvents */
	EventTimer() : mStopped(true) {
		cudaEventCreate(&mStart);
		cudaEventCreate(&mStop);
	}
	/** Destructor. Destroys the EventTimer::mStart and EventTimer::mStop cudaEvents */
	~EventTimer() {
		cudaEventDestroy(mStart);
		cudaEventDestroy(mStop);
	}
	/** Starts the timer. Checks first EventTimer::mStopped to see if timer is not already running.
	 *  Sets EventTimer::mStopped to false.
	 *  @param s specifies the cudaStream, defaults to zero.
	 */
	void start(cudaStream_t s = 0 ) {
		#ifdef CUDA_TIME
		if(!mStopped) {
			std::cerr << "error: EventTimer already running" ;
			exit(EXIT_FAILURE);
		}
		myCudaSafeCall(cudaEventRecord(mStart, s)); mStopped = false; 
		#endif
		return;
	}
	/** Stops the timer. Checks first EventTimer::mStopped to see if timer is not already stopped.
	 *  Sets EventTimer::mStopped to true.
	 *  @param s specifies the cudaStream, defaults to zero.
	 */
	void stop(cudaStream_t s = 0 )  {
		#ifdef CUDA_TIME
		if(mStopped) {
			std::cerr << "error: EventTimer already stopped" ;
			exit(EXIT_FAILURE);
		}
		myCudaSafeCall(cudaEventRecord(mStop, s)); mStopped = true;
		#endif
		return;
	}
	
	/** Returns the elapsed time. Checks first EventTimer::mStopped to see if timer stopped.
	 * Then synchronizes cudaEvents and returns elapsed time in ms.
	 * @return the elapsed time
	 */
	float elapsed() {
		#ifdef CUDA_TIME
		if (!mStopped) {
			std::cerr << "error: EventTimer still running" ;
			exit(EXIT_FAILURE);
		}
		myCudaSafeCall(cudaEventSynchronize(mStop));
		float elapsed = 0.0f;
		myCudaSafeCall(cudaEventElapsedTime(&elapsed, mStart, mStop));
		return elapsed;
		#else
		return 0.0f;
		#endif
	}
private:
	bool mStopped; /**< bool with the state of the timer */
	cudaEvent_t mStart; /**< cudaEvent to store the start of the timer */
	cudaEvent_t mStop; /**< cudaEvent to store the stop of the timer */
};
#endif
