#pragma once
#ifndef SELECT_DEVICE_H
#define SELECT_DEVICE_H

#include<stdlib.h>
#include<stdio.h>
#include<string.h>

#include "cuda_errorcheck.cuh"

/** 	@file select_device.h
	Functions to select device. Ripped from the cutil library and made some small improvements.
 */ 

#ifndef MAX
/**	@def MAX(a,b)
	Returns the maximum of @a a and @a b. 
	Type safe and avoids double evaluation
*/
#define MAX(a,b) \
       ({ __typeof__ (a) _a = (a); \
           __typeof__ (b) _b = (b); \
         _a > _b ? _a : _b; })
#endif

/**
	Converts CC into # of SP/cores per SM.
	Maps the compute capability (CC) to the the number of streaming processors (SP) 
	per streaming multiprocessors (SM).
	@param major major version of compute capability	
	@param minor minor version of compute capability	
	@returns number of SP/cores per SM
*/
inline int _ConvertSMVer2Cores(int major, int minor)
{
    // Defines for GPU Architecture types (using the SM version to determine the # of cores per SM
    typedef struct
    {
        int SM; // 0xMm (hexidecimal notation), M = SM Major version, and m = SM minor version
        int Cores;
    } sSMtoCores;

    sSMtoCores nGpuArchCoresPerSM[] =
    {
        { 0x10,  8 }, // Tesla Generation (SM 1.0) G80 class
        { 0x11,  8 }, // Tesla Generation (SM 1.1) G8x class
        { 0x12,  8 }, // Tesla Generation (SM 1.2) G9x class
        { 0x13,  8 }, // Tesla Generation (SM 1.3) GT200 class
        { 0x20, 32 }, // Fermi Generation (SM 2.0) GF100 class
        { 0x21, 48 }, // Fermi Generation (SM 2.1) GF10x class
        { 0x30, 192}, // Kepler Generation (SM 3.0) GK10x class
        { 0x35, 192}, // Kepler Generation (SM 3.5) GK11x class
        {   -1, -1 }
    };

    int index = 0;

    while (nGpuArchCoresPerSM[index].SM != -1)
    {
        if (nGpuArchCoresPerSM[index].SM == ((major << 4) + minor))
        {
            return nGpuArchCoresPerSM[index].Cores;
        }

        index++;
    }

    // If we don't find the values, we default use the previous one to run properly
    printf("MapSMtoCores for SM %d.%d is undefined.  Default to use %d Cores/SM\n", major, minor, nGpuArchCoresPerSM[7].Cores);
    return nGpuArchCoresPerSM[7].Cores;
}

/**
	Initializes the CUDA Device. 
	Checks if device supports CUDA and if the compute mode is not prohibited.
	@param devID device ID
	@returns device ID on success, -1 on failure
	@see findCudaDevice
*/
inline int gpuDeviceInit(int devID)
{

    cudaDeviceProp deviceProp;
    myCudaSafeCall(cudaGetDeviceProperties(&deviceProp, devID));

    if (deviceProp.computeMode == cudaComputeModeProhibited)
    {
        fprintf(stderr, "Error: device is running in <Compute Mode Prohibited>, no threads can use ::cudaSetDevice().\n");
        return -1;
    }

    if (deviceProp.major < 1)
    {
        fprintf(stderr, "gpuDeviceInit(): GPU device does not support CUDA.\n");
        exit(EXIT_FAILURE);
    }

    myCudaSafeCall(cudaSetDevice(devID));
    printf("gpuDeviceInit() CUDA Device [%d]: \"%s\n", devID, deviceProp.name);

    return devID;
}

/**
	Returns device ID of fastest device.
	First determines the compute capability of all devices, then returns the one with the most GFLOPS.
	Devices with compute capability > 2 beat devices with lower compute capability regardless of GFLOPS.
	@warning The original code in cutil contains a bug, @a max_perf_device should be initialized to -1, this is fixed in this version
	to avoid crashing on runtime on a system without CUDA cards
	@returns device ID of fastest device
	@see findCudaDevice
	@see _ConvertSMVer2Cores
*/
inline int gpuGetMaxGflopsDeviceId()
{
    int current_device     = 0, sm_per_multiproc  = 0;
    int max_compute_perf   = 0, max_perf_device   = -1;
    int device_count       = 0, best_SM_arch      = 0;
    cudaDeviceProp deviceProp;
    cudaGetDeviceCount(&device_count);

    // Find the best major SM Architecture GPU device
    while (current_device < device_count)
    {
        cudaGetDeviceProperties(&deviceProp, current_device);

        // If this GPU is not running on Compute Mode prohibited, then we can add it to the list
        if (deviceProp.computeMode != cudaComputeModeProhibited)
        {
            if (deviceProp.major > 0 && deviceProp.major < 9999)
            {
                best_SM_arch = MAX(best_SM_arch, deviceProp.major);
            }
        }

        current_device++;
    }

    // Find the best CUDA capable GPU device
    current_device = 0;

    while (current_device < device_count)
    {
        cudaGetDeviceProperties(&deviceProp, current_device);

        // If this GPU is not running on Compute Mode prohibited, then we can add it to the list
        if (deviceProp.computeMode != cudaComputeModeProhibited)
        {
            if (deviceProp.major == 9999 && deviceProp.minor == 9999)
            {
                sm_per_multiproc = 1;
            }
            else
            {
                sm_per_multiproc = _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor);
            }

            int compute_perf  = deviceProp.multiProcessorCount * sm_per_multiproc * deviceProp.clockRate;

            if (compute_perf  > max_compute_perf)
            {
                // If we find GPU with SM major > 2, search only these
                if (best_SM_arch > 2)
                {
                    // If our device==dest_SM_arch, choose this, or else pass
                    if (deviceProp.major == best_SM_arch)
                    {
                        max_compute_perf  = compute_perf;
                        max_perf_device   = current_device;
                    }
                }
                else
                {
                    max_compute_perf  = compute_perf;
                    max_perf_device   = current_device;
                }
            }
        }

        ++current_device;
    }

    return max_perf_device;
}


/**
	Finds CUDA device.
	If no commmand line argument is given calls ::gpuGetMaxGflopsDeviceId to determine the fastest device, then initializes it.
	If command line argument is given, calls ::gpuDeviceInit which checks if the device is available and if so initializes it. 
	@see gpuDeviceInit
	@see gpuGetMaxGflopsDeviceId
	@param CmdArgDevice the device ID given on the command line, -1 if none is given
	@returns the device ID of the initialized device
*/
inline int findCudaDevice(int CmdArgDevice)
{
    int devID = CmdArgDevice; 

    int deviceCount;
    myCudaSafeCall(cudaGetDeviceCount(&deviceCount));

    if (deviceCount == 0)
    {
        fprintf(stderr, "findCudaDevice() CUDA error: no devices supporting CUDA.\n");
        exit(EXIT_FAILURE);
    }

    if (devID > deviceCount-1)
    {
        fprintf(stderr, "findCudaDevice() CUDA error: %d CUDA capable GPU device(s) detected.\n", deviceCount);
        fprintf(stderr, "device %d is not a valid GPU device.\n", devID);
        exit(EXIT_FAILURE);
    }

    // If the command-line has a device number specified, use it
    if (devID >= 0)
    {
            devID = gpuDeviceInit(devID);
            if (devID < 0) {
                printf("exiting...\n");
                exit(EXIT_FAILURE);
            }
    } else {
        // Otherwise pick the device with highest Gflops/s
        devID = gpuGetMaxGflopsDeviceId();
        myCudaSafeCall(cudaSetDevice(devID));
    }

    cudaDeviceProp deviceProp;
    myCudaSafeCall(cudaGetDeviceProperties(&deviceProp, devID));
    printf("GPU Device %d: \"%s\" with compute capability %d.%d\n", 
	devID, deviceProp.name, deviceProp.major, deviceProp.minor);

    return devID;
}

/**
	Checks card for compute capability.
	Compute capability of device should be greater or equal to the provided compute capability.
	Device should be initialized before this function is called.
	@see findCudaDevice
	@param major_version major compute capability
	@param minor_version minor compute capability
	@returns @a true if card supports compute capability, @a false if not
*/
inline bool checkCudaCapabilities(int major_version, int minor_version)
{
    cudaDeviceProp deviceProp;
    deviceProp.major = 0;
    deviceProp.minor = 0;
    int dev;

    myCudaSafeCall(cudaGetDevice(&dev));
    myCudaSafeCall(cudaGetDeviceProperties(&deviceProp, dev));

    if ((deviceProp.major > major_version) ||
        (deviceProp.major == major_version && deviceProp.minor >= minor_version))
    {
        printf("> Device %d: <%16s >, Compute SM %d.%d detected\n", dev, deviceProp.name, deviceProp.major, deviceProp.minor);
        return true;
    }
    else
    {
        printf("No GPU device was found that can support CUDA compute capability %d.%d.\n", major_version, minor_version);
        return false;
    }
}
#endif
